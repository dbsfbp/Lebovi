<?php
// No direct access
defined('_JEXEC') or die;
if ($elements != "Can't get slides"):
    ?>

    <?
    $count = 0;
    foreach ($elements as $i => $el) {
        $count++;
    }
    ?>


    <? // print_r($elements)
    ?>

    <? foreach ($elements as $i => $el): ?>
    <div class="col-md-<? if ((12 / $count) < 1) {
        echo(12 / ($count % 12));
    } else {
        echo 12 / $count;
    } ?> text-center course">
        <div class="course-image"
             style="background-image: url(<?= $el->course_img ?>)"></div>
        <div class="course-content-header">
            <?= $el->course_name ?>
        </div>
        <div class="course-content">
            <?= $el->course_descr ?>
        </div>
        <br>
        <a href="<?= $el->course_link ?>" class="btn btn-danger">Подробней</a>
    </div>
<? endforeach ?>
<? endif ?>

<!--<div class="have-question">-->
<!--    <h2 class="">--><? //=$have_question_heading?><!--</h2>-->
<!---->
<!--    <p class="flat-lh-28">-->
<!--	    --><? //=$have_question_text?>
<!--    </p>-->
<!---->
<!--    <div class="flat-button-container">-->
<!--        <a class="read-more" href="--><? //=$have_question_menuitem_link?><!--">--><? //=$have_question_link_text?><!--</a>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--<div class="news-letter-form">-->
<!--    <div class="widget-mailchimb">-->
<!--        <h1 class="widget-title">--><? //=$news_letter_form_heading?><!--</h1>-->
<!--        <p>--><? //=$news_letter_form_text?><!--</p>-->
<!--        <form method="post" action="--><? //=$action?><!--" sendTo="--><? //=$email?><!--" id="subscribe-form" data-mailchimp="true">-->
<!--            <div id="subscribe-content">-->
<!--                <div class="input-wrap email">-->
<!--                    <input type="text" id="subscribe-email" name="subscribe-email"-->
<!--                           placeholder="--><? //=$news_letter_form_input_text?><!--">-->
<!--                </div>-->
<!--                <div class="button-wrap">-->
<!--                    <button type="button" id="subscribe-button" class="subscribe-button"-->
<!--                            title="Subscribe now"> --><? //=$news_letter_form_button_text?>
<!--                    </button>-->
<!--                </div>-->
<!--            </div>-->
<!--            <div id="subscribe-msg">-->
<!--                <div id="subscribe-msg-fail" style="display:none">--><? //=$news_letter_form_fail_message?><!--</div>-->
<!--                <div id="subscribe-msg-succes" style="display:none">--><? //=$news_letter_form_success_message?><!--</div>-->
<!--            </div>-->
<!--        </form>-->
<!---->
<!--    </div>-->
<!--</div>-->