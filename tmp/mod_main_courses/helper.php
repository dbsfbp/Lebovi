<?php
/**
 * @package       RSPageBuilder!
 * @copyright (C) 2016 www.rsjoomla.com
 * @license       GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */

// No direct access
defined('_JEXEC') or die ('Restricted access');
?>

<?php

if (isset($params))
{
	$have_question_menuitem = $params->get('have_question_menuitem', 'Error in subscribe module');
	$have_question_menuitem_link = JRoute::_('index.php?Itemid=' . $have_question_menuitem);
	$have_question_link_text = $params->get('have_question_link_text', 'Error in subscribe module');
}

$app = JFactory::getApplication();
$action = JURI::base() . 'templates/' . $app->getTemplate() . '/contact/contact-process.php';