<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$id = '';

if ($tagId = $params->get('tag_id', '')) {
    $id = ' id="' . $tagId . '"';
}

// The menu class is deprecated. Use nav instead
?>

<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu"
        aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div id="mainMenu" class="collapse navbar-collapse justify-content-center">
    <ul class="navbar-nav">
        <?php foreach ($list

                       as $i => &$item) {
            $class = 'nav-item';

            if ($item->id == $default_id) {
                $class .= ' default';
            }
            if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id)) {
                $class .= ' home';
            }

            if ($item->deeper) {
                $class .= ' dropdown';
            }

            //            print_r($item);
            if ($item->level == 1) {
                switch ($item->type) :
                    case 'separator':
                    case 'component':
                    case 'heading':
                    case 'url':
                        echo '<li class="' . $class . '">';
                        break;
                    default:
                        echo '<li class="' . $class . '">';
                        break;
                endswitch;

                if ($item->deeper) {
                    echo "<a class='nav-link dropdown-toggle main-menu-item' data-toggle='dropdown' role='button'
               aria-haspopup='true' aria-expanded='false' href='#'>" . $item->title . "</a>";
                } else {
                    echo "<a class='nav-link main-menu-item' href='" . $item->link . "'>" . $item->title . "</a>";
                }
            } else {
                echo '<a class="dropdown-item dropdown-item-main" href="/kurs_nachalnij">Курс "Начальный"</a>';
            }


            if ($item->deeper) {
                echo '<div class="dropdown-menu dropdown-main-menu text-right">';
            } // The next item is shallower.
            elseif ($item->shallower) {
                echo '</div>';
                echo str_repeat('</div></li>', $item->level_diff);
            } // The next item is on the same level.
//            else {
//                echo '</div>';
//            }
        }
        ?>

        <div id="moreBtn" class="nav-item dropdown hidden">
            <a class="nav-link dropdown-toggle main-menu-item" href="#" data-toggle="dropdown" role="button"
               aria-haspopup="true" aria-expanded="false">Ещё...</a>
            <div id="moreBtnList" class="dropdown-menu dropdown-main-menu text-right">
                <a class="dropdown-item dropdown-item-main hidden" href="#">Главная</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">Курсы</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">Галерея</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">О курсах</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">Контакты</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">Новости</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">МАРАФОН</a>
                <a class="dropdown-item dropdown-item-main hidden" href="#">Online ОбучениеРАФОН</a>
            </div>
        </div>


        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link active main-menu-item" href="#">Главная</a>-->
        <!--        </li>-->
        <!--        <li class="nav-item dropdown">-->
        <!--            <a class="nav-link dropdown-toggle main-menu-item" data-toggle="dropdown" href="#" role="button"-->
        <!--               aria-haspopup="true" aria-expanded="false">Курсы</a>-->
        <!--            <div class="dropdown-menu dropdown-main-menu text-right">-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/kurs_nachalnij">Курс "Начальный"</a>-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/kurs_srednii">Курс "Средний"</a>-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/kurs_profi">Курс "Профи"</a>-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/kurs_modeling">Курс конструирования и-->
        <!--                    моделирование одежды</a>-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/kurs_tricotaz">Курс "Трикотаж"</a>-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/kurs_belje">Курс "Белье"</a>-->
        <!--                <a class="dropdown-item dropdown-item-main" href="/master_klass">Мастер классы</a>-->
        <!--            </div>-->
        <!--        </li>-->
        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link main-menu-item" href="#">Галерея</a>-->
        <!--        </li>-->
        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link main-menu-item" href="#">О курсах</a>-->
        <!--        </li>-->
        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link main-menu-item" href="#">Контакты</a>-->
        <!--        </li>-->
        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link main-menu-item" href="#">Новости</a>-->
        <!--        </li>-->
        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link main-menu-item" href="#">МАРАФОН</a>-->
        <!--        </li>-->
        <!--        <li class="nav-item">-->
        <!--            <a class="nav-link main-menu-item" href="#">Online Обучение</a>-->
        <!--        </li>-->
        <!---->
        <!--        <div id="moreBtn" class="nav-item dropdown hidden">-->
        <!--            <a class="nav-link dropdown-toggle main-menu-item" href="#" data-toggle="dropdown" role="button"-->
        <!--               aria-haspopup="true" aria-expanded="false">Ещё...</a>-->
        <!--            <div id="moreBtnList" class="dropdown-menu dropdown-main-menu text-right">-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">Главная</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">Курсы</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">Галерея</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">О курсах</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">Контакты</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">Новости</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">МАРАФОН</a>-->
        <!--                <a class="dropdown-item dropdown-item-main hidden" href="#">Online ОбучениеРАФОН</a>-->
        <!--            </div>-->
        <!--        </div>-->
    </ul>
</div>

<!--<ul class="menu--><?php //echo $class_sfx; ?><!--"--><?php //echo $id; ?><!-->
<!--    --><?php //foreach ($list as $i => &$item) {
//        $class = 'item-' . $item->id;
//
//        if ($item->id == $default_id) {
//            $class .= ' default';
//        }
//        if ($item->id == $active_id || ($item->type === 'alias' && $item->params->get('aliasoptions') == $active_id)) {
//            $class .= ' home';
//        }
//
//        if ($item->deeper) {
//            $class .= ' has-sub';
//        }
//        echo '<li class="' . $class . '">';
//
//        switch ($item->type) :
//            case 'separator':
//            case 'component':
//            case 'heading':
//            case 'url':
//                require JModuleHelper::getLayoutPath('mod_menu', 'default_' . $item->type);
//                break;
//
//            default:
//                require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
//                break;
//        endswitch;
//
//        if ($item->deeper) {
//            echo '<ul class="submenu">';
//        } // The next item is shallower.
//        elseif ($item->shallower) {
//            echo '</li>';
//            echo str_repeat('</ul></li>', $item->level_diff);
//        } // The next item is on the same level.
//        else {
//            echo '</li>';
//        }
//    }
//    ?>
<!--</ul>-->
