jQuery.noConflict();
jQuery(document).ready(function($) {
    var mainMenu = document.getElementById('mainMenu');
    var moreBtn = document.getElementById('moreBtn');
    var moreBtnList = $("#moreBtnList").find("a");
    var mainMenuContainer = $("#mainMenuContainer")[0];

    var menuOffsetTop = $(mainMenuContainer).offset().top;

    var elementsCount = 0;
    var menuElementsWidth = 0;

    console.log(moreBtnList);

    $("#mainMenu li").each(function(index, el) {
        // console.log($(el).width());
        menuElementsWidth += $(el).width();
        elementsCount += 1;
    });

    function UpdateMenuElements() {
        var currentWidth = 0;
        var maxWidth = $(document).width();

        $("#mainMenu li").each(function(index, el) {
            var elementWidth = $(el).width();

            if (currentWidth + elementWidth + 130 > maxWidth) {
                if (!el.classList.contains("hidden") && $(document).width() > 555) {
                    el.classList.add("hidden");

                    if (moreBtnList[index].classList.contains("hidden")) {
                        moreBtnList[index].classList.remove("hidden");
                    }
                }
            } else {
                currentWidth += elementWidth;

                if (el.classList.contains("hidden")) {
                    el.classList.remove("hidden");

                    if (!moreBtnList[index].classList.contains("hidden")) {
                        moreBtnList[index].classList.add("hidden");
                    }
                }
            }
        });

        UpdateMoreElements();
    }

    function UpdateMoreElements() {

    }

    function checkMenuWidth() {
        if(menuElementsWidth + 130 > $(document).width() && $(document).width() > 575) {
            if (moreBtn.classList.contains("hidden")) {
                moreBtn.classList.remove("hidden");
            }
        } else {
            if (!moreBtn.classList.contains("hidden")) {
                moreBtn.classList.add("hidden");
            }
        }

        UpdateMenuElements();
    }

    window.onresize = function(event) {
        checkMenuWidth();
    };

    checkMenuWidth();

    $(window).scroll(function(){
        var scrollPos = $(document).scrollTop();
        // console.log(scrollPos);

        if(scrollPos > 10) {
            $("#go-top")[0].classList.remove("go-top-hidden");
        } else {
            $("#go-top")[0].classList.add("go-top-hidden");
        }

        if (scrollPos >= menuOffsetTop) {
            if(!(mainMenuContainer.classList.contains("fixed-top"))) {
                mainMenuContainer.classList.add("fixed-top");
                mainMenuContainer.classList.add("fixed-menu-background");

                $("#menu-place")[0].classList.add("menu-place");
            }
        } else {
            if(mainMenuContainer.classList.contains("fixed-top")) {
                mainMenuContainer.classList.remove("fixed-top");
                mainMenuContainer.classList.remove("fixed-menu-background");

                $("#menu-place")[0].classList.remove("menu-place");
            }
        }
    });
});