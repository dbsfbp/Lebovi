jQuery(document).ready(function ($) {
    // change slider buttons
    $(".owl-prev").html("<i class=\"fa fa-chevron-left\" aria-hidden=\"true\"></i>");
    $(".owl-next").html("<i class=\"fa fa-chevron-right\" aria-hidden=\"true\"></i>");
});